#!/usr/bin/env bash
set -x
set -eo pipefail

DB_HOST="${POSTGRES_HOST:=localhost}"
DB_USER="${POSTGRES_USER:=user}"
DB_PASSWORD="${POSTGRES_PASSWORD:=pass}"
DB_NAME="${POSTGRES_DB:=logs}"
DB_PORT="${POSTGRES_PORT:=5432}"

if [[ -z "${SKIP_DOCKER}" ]]
then
  docker run \
    -e POSTGRES_USER=${DB_USER} \
    -e POSTGRES_PASSWORD=${DB_PASSWORD} \
    -e POSTGRES_DB=${DB_NAME} \
    -p "${DB_PORT}":5432 \
    -d postgres \
    postgres -N 1000
    # ^ Increasing number of connections to 1000 for testing purposes

fi

until PGPASSWORD="${DB_PASSWORD}" psql -h "${DB_HOST}" -U "${DB_USER}" -p "${DB_PORT}" -d "${DB_NAME}" -c '\q'; do
  >&2 echo "Postgres is unavailable. Trying again in 1 sec."
  sleep 1
done

>&2 echo "Postgres is running on port ${DB_PORT}."

export DATABASE_URL="postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}"

[ ! -x "$(which sqlx)" ] && {
    >&2 echo "sqlx-cli not installed. Installing...";
    cargo install sqlx-cli --no-default-features --features rustls,sqlite
}
>&2 echo "Creating database" && sqlx database create 
>&2 echo "Running migrations" && sqlx migrate run

>&2 echo "Finished migrations!"
