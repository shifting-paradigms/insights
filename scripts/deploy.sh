#!/usr/bin/env bash

set -x
set -eo pipefail

# check if force flag is set
if [[ $1 == "--force" ]]; then
  force=true
else
  force=false
fi


if [[ $force == false ]]; then
  branch=$(git rev-parse --abbrev-ref HEAD)
  if [[ $branch != "rc/"* ]]; then
    echo "Not on a release branch, aborting"
    exit 1
  fi

  latest_commit=$(git rev-parse HEAD)
  if [[ $latest_commit != $(git rev-parse $branch) ]]; then
    echo "Not on the latest commit on the branch, aborting"
    exit 1
  fi

  version_tag=$(git describe --tags --exact-match)
  if [ -z "$version_tag" ]; then
    echo "No tag found for current commit, aborting"
    exit 1
  fi

  pkg_version=$(grep -oP '(?<=^version = ")[^"]*' Cargo.toml)
  if [[ $version_tag != $pkg_version ]]; then
    echo "Version in Cargo.toml does not match git tag, aborting"
    exit 1
  fi
fi

cargo fmt -- --check && \
  cargo clippy -- -D warnings && \
  cargo test && \
  sudo docker run --rm -v "$PWD":/code -w /code rust:bullseye /bin/sh -c 'cargo install sqlx-cli && SQLX_OFFLINE=true cargo build --release' && \
  rsync --checksum --verbose --progress --human-readable -e 'ssh -p 8022' \
    target/release/insights \
    root@116.202.9.87:/root/insights && \
  ssh -p 8022 root@116.202.9.87 "rm -rf /usr/local/bin/insights" && \
  ssh -p 8022 root@116.202.9.87 "mv /root/insights /usr/local/bin/insights" && \
  ssh -p 8022 root@116.202.9.87 "systemctl restart insights"
