# Insights
A light-weight, minimalistic and pragmatic telemetry server. Keeps it simple. Speaks truth.

Works well with:
- [jump-start](https://gitlab.com/shifting-paradigms/jump-start) - A starter template for observable high-performance web services.

## Documentation
When working with the library crate you can build the documentation:
```bash
cargo doc --document-private-items --no-deps --open
```

## Usage
```bash
# run as configured in config/base.toml + config/local.toml
# optional: config/development.toml / config/production.toml
cargo run

# use env vars
APP_APP_PORT=8012 APP_LOG_LEVEL=info cargo run

# For testing the endpoint via cli I recommend websocat + jq
# https://crates.io/crates/websocat
# https://stedolan.github.io/jq/
cargo install websocat

# with `token = "awesome"` in ./config/local.toml
# or `APP_AUTH_TOKEN=awesome cargo run`
websocat ws://127.0.0.1:8081/insights -H='Authorization: Bearer awesome' | jq

# send a stream config
{"log":{"interval":1,"start_timestamp":"2021-05-01T00:00:00Z"},"host":{"interval":5},"api":{"interval":20}}
```

## Description

Collects metrics and sends them to connected clients via Websocket.
Before receiving any packages on the client, each client has to provide a stream configuration.

### Configuration example
```javascript
// Example of full configuration object
{
  "log": {
    "interval": 1,
    "start_timestamp": "2021-05-01T00:00:00Z"
  },
  "host": {
    "interval": 5
  },
  "api": {
    "interval": 20
  }
}
// Partial configuration can also be applied
// In this case no host metrics will be streamed
{
  "log": {
    "interval": 1,
    "start_timestamp": "2021-05-01T00:00:00Z"
  },
  "api": {
    "interval": 20
  }
}
```

```javascript
// Example of a Package carrying HostMetrics
{
  "id": "cf184882-9961-4b41-a17d-3acb2dbd1160",
  "kind": "HostMetrics",
  "data": {
    "timestamp": 1651932214, // seconds
    "name": "Darwin",
    "os_version": "MacOS 11.6.5 ",
    "kernel": "20.6.0",
    "hostname": "Users-MBP",
    "uptime": 1116306, // seconds
    "cpu": {
      "brand": "Intel(R) Core(TM) i7-4870HQ CPU @ 2.50GHz",
      "cores": 4,
      "current_load": 6.88, // percent
      "avg_load_one": 3.31, // percent
      "avg_load_five": 2.23, // percent
      "avg_load_fifteen": 2.06 // percent
    },
    "memory": {
      "total": 17179, // MB
      "used": 16774, // MB
      "free": 405, // MB
      "available": 3410 // MB
    },
    "disk": {
      "total": 499933, // MB
      "used": 314118 // MB
    },
    "io": {
      "received": 2048, // B
      "transmitted": 1024 // B
    }
  }
}

// Example of a Package carrying LogEntry (sourced from file)
// https://gitlab.com/shifting-paradigms/jump-start
{
  "id": "fbc2b3a1-cf18-464f-9898-d9a68c4289ff",
  "kind": "LogEntry",
  "data": {
    "level": "DEBUG",
    "message": "GET /metrics",
    "target": "jump_start::telemetry",
    "span": {
      "id": "a6f6b537-48f8-4d1e-bb4f-d389e11f176b",
      "name": "http-request"
    },
    "timestamp": "2022-05-08T08:55:58.108695Z",
    "created_at": "2022-05-12T08:55:58.108695Z" // <- use this for client-side bookmarking
  }
}

// Example of a Package carrying ApiMetrics (sourced from prometheus endpoint)
// https://gitlab.com/shifting-paradigms/jump-start
{
  "id": "7496d3f2-0a24-4c98-a06c-45845abdcb71",
  "kind": "ApiMetrics",
  "data": {
    "http_requests_total": [
      {
        "name": "http_requests_total",
        "method": "POST",
        "path": "/subscriptions",
        "status": 422,
        "le": null,
        "count": 2
      },
      {
        "name": "http_requests_total",
        "method": "GET",
        "path": "/metrics",
        "status": 200,
        "le": null,
        "count": 4938
      },
      {
        "name": "http_requests_total",
        "method": "GET",
        "path": "/heartbeat",
        "status": 200,
        "le": null,
        "count": 7
      }
    ],
    "http_requests_duration_seconds": [
      {
        "name": "http_requests_duration_seconds_bucket",
        "method": "GET",
        "path": "/heartbeat",
        "status": 200,
        "le": 0.005,
        "count": 7
      },
      // ...
      {
        "name": "http_requests_duration_seconds_bucket",
        "method": "GET",
        "path": "/heartbeat",
        "status": 200,
        "le": 0.5,
        "count": 7
      },
      // ...
      {
        "name": "http_requests_duration_seconds_sum",
        "method": "GET",
        "path": "/heartbeat",
        "status": 200,
        "le": null,
        "count": 0.000660703
      },
      {
        "name": "http_requests_duration_seconds_count",
        "method": "GET",
        "path": "/heartbeat",
        "status": 200,
        "le": null,
        "count": 7
      }
    ]
  }
}
```
