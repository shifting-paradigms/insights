CREATE TABLE
    IF NOT EXISTS logs (
        id SERIAL PRIMARY KEY,
        timestamp TIMESTAMPTZ NOT NULL,
        level VARCHAR(10) NOT NULL,
        message TEXT NOT NULL,
        target VARCHAR(100),
        span JSONB DEFAULT '{}':: JSONB,
        created_at TIMESTAMPTZ NOT NULL,
        UNIQUE (
            timestamp,
            level,
            message,
            target,
            span
        )
    );