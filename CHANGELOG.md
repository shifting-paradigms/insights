# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.3.8] - 2023-02-27
### Added
- reduce database logging verbosity
- ability to authenticate with bearer token at prometheus endpoints
### Fixed
- avoid client flooding
- calculation of network traffic metrics
- calculation of disk usage metrics
- wrong interpretation of cpu metrics
- handling potential errors

## [1.0.0] - 2023-01-30
### Added
- Metric types can now be streamed non-sequencial
### Changed
- Client needs to provide an initial StreamConfig

## [0.7.0] - 2023-01-25
### Added
- add persistant storage for log files via postgresql

## [0.6.0] - 2022-05-24
### Added
- skip fetching of api metrics endpoint by specifiable factor

### Fixed
- package loss due to slower client side performance

### Changed
- log entries are now sent last for better ux

## [0.5.0] - 2022-05-14
### Added
- increased logging verbosity by adding http trace layer
- websocket endpoint is now protected via a configurable bearer token.

### Changed
- instead of sourcing one log file, a file handler sources the latest file from a specified directory

## [0.4.0] - 2022-05-13
### Changed
- collection of api metrics is optional
- collection of logs entries is optional

### Fixed
- websocket stops sending packages when prometheus endpoint is not reachable

## [0.3.0] - 2022-05-12
### Added
- collect api metrics from specified prometheus endpoint

### Changed
- collection of host metrics is optional

## [0.2.0] - 2022-05-08
### Added
- collect log entries from specified file

## [0.1.0] - 2022-05-07
### Added
- add websocket endpoint
- collect metrics from host machine
- load app configuration from files and/or env vars
- basic web server with timeout, fallback and health check
- graceful shutdown
- display tracing log on stdout