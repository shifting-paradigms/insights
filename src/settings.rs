//! Settings for the application
//!
//! This module contains the settings for the application. It uses the `config` crate to load
//! the settings from the environment, the configuration files and the local configuration file.
//!

use config::{Config, ConfigError, Environment, File};
use hyper::Uri;
use secrecy::SecretString;
use serde::Deserialize;
use std::{env, str::FromStr};

/// The settings for the application.
#[derive(Clone, Debug, Deserialize)]
#[allow(unused)]
pub struct App {
    /// The host of the application.
    pub host: String,
    /// The port of the application.
    pub port: u16,
}

impl App {
    /// Get the address of the application.
    ///
    /// This method returns the address of the application as a string,
    /// containing the host and the port.
    pub fn address(&self) -> String {
        format!("{}:{}", self.host, self.port)
    }
}

/// The settings for the authentication.
#[derive(Clone, Debug, Deserialize)]
#[allow(unused)]
pub struct Auth {
    /// The secret key of the application.
    ///
    /// This value is used as the bearer token for the authentication.
    pub token: SecretString,
}

/// The settings for the database.
#[derive(Clone, Debug, Deserialize)]
#[allow(unused)]
pub struct Database {
    /// The host of the database.
    pub host: String,
    /// The port of the database.
    pub port: u16,
    /// The name of the database.
    pub name: String,
    /// The user of the database.
    pub user: String,
    /// The password of the database.
    pub password: SecretString,
    /// The log level of the database.
    pub log_level: String,
}

/// The settings for the sources.
///
/// This type is used to configure the sources of the metrics.
/// The sources are the Prometheus exporter, the log files and the API.
#[derive(Clone, Debug, Deserialize)]
#[allow(unused)]
pub struct Sources {
    /// Whether operating system metrics should be collected from the host machine.
    pub from_host: bool,
    /// Whether log entries should be collected from log files.
    pub from_log: bool,
    /// The path to the directory containing the log files.
    pub log_dir: String,
    /// Whether api metrics should be collected from a Prometheus exporter.
    pub from_api: bool,
    /// The uri of the Prometheus exporter.
    pub api_prometheus_exporter_uri: String,
    /// The prometheus api authentication token.
    pub api_prometheus_auth_token: SecretString,
}

/// The settings for the logging of the application.
#[derive(Clone, Debug, Deserialize)]
#[allow(unused)]
pub struct Log {
    /// The level of the application logging.
    pub level: String,
}

/// The combined settings for the application.
#[derive(Clone, Debug, Deserialize)]
#[allow(unused)]
pub struct Settings {
    /// The settings for the applications server.
    pub app: App,
    /// The settings for the authentication.
    pub auth: Auth,
    /// The settings for the database.
    pub database: Database,
    /// The settings for the logging.
    pub log: Log,
    /// The settings for the sources.
    pub sources: Sources,
}

impl Settings {
    /// Create a new instance of application settings.
    ///
    /// This method creates a new instance of the application settings.
    /// It uses the `config` crate to load the settings from the environment,
    /// the configuration files and the local configuration files.
    pub fn new() -> Result<Self, ConfigError> {
        let app_environment = env::var("APP_ENV").unwrap_or_else(|_| "development".into());

        let settings = Config::builder()
            .add_source(File::with_name("config/base"))
            .add_source(File::with_name(&format!("config/{}", app_environment)).required(false))
            // Add an optional local configuration
            // This file shouldn't be checked in to git
            .add_source(File::with_name("config/local").required(false))
            // Eg.. APP_APP_PORT=8080 APP_LOG_LEVEL=info
            .add_source(Environment::with_prefix("app").separator("_"))
            .build()
            .expect("failed to build config");

        settings.try_deserialize()
    }
}

/// Test the configuration of the sources.
///
/// This function tests the configuration of the sources
/// to spot potential problems during the initial startup.
/// It checks if the log directory exists if the log source is enabled.
/// It checks if the Prometheus exporter uri is valid if the API source is enabled.
#[tracing::instrument(skip_all)]
pub fn test_source_config(sources: &Sources) -> anyhow::Result<()> {
    // TODO: check only if log dir exists
    if sources.from_log {
        if sources.log_dir.is_empty() {
            tracing::error!("No value for log_dir provided. Please check your configuration.");
            return Err(anyhow::anyhow!(""));
        }

        if !std::path::Path::new(&sources.log_dir).exists() {
            tracing::error!(
                "Provided log directory does not exist. Please check your configuration.",
            );
            return Err(anyhow::anyhow!(""));
        }
    }
    if sources.from_api && Uri::from_str(&sources.api_prometheus_exporter_uri).is_err() {
        tracing::error!(
            "invalid uri: \"{}\". Please check your configuration.",
            &sources.api_prometheus_exporter_uri
        );
        return Err(anyhow::anyhow!(""));
    }
    Ok(())
}
