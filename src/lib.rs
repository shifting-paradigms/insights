//! # The `insights` application library crate.
//!
//! This crate contains the logic for the `insights` application.
//!
//! ## Overview
//!
//! The `insights` application is a web application that collects metrics from
//! different sources and provides them to the user via a web interface.
//!
//! The application is written in Rust and uses the following crates:
//!
//! - [axum](https://crates.io/crates/axum) for the web server
//! - [sqlx](https://crates.io/crates/sqlx) for the database
//! - [tracing](https://crates.io/crates/tracing) for the logging
//! - [tower](https://crates.io/crates/tower) for the middleware
//! - [secrecy](https://crates.io/crates/secrecy) for the secrets
//! - [serde](https://crates.io/crates/serde) for the serialization
//! - [config](https://crates.io/crates/config) for the configuration
//! - [metrics](https://crates.io/crates/metrics) for api metrics
//! - [metrics-exporter-prometheus](https://crates.io/crates/metrics-exporter-prometheus) for the Prometheus exporter
//! - [sysinfo](https://crates.io/crates/sysinfo) for the host metrics
//!

pub mod database;
mod errors;
pub mod metrics;
pub mod settings;
pub mod shutdown;
pub mod startup;
pub mod telemetry;
pub mod websocket;
