use insights::{database::DynDatabase, metrics::log, settings, shutdown, startup, telemetry};
use secrecy::ExposeSecret;
use sqlx::{
    postgres::{PgConnectOptions, PgPoolOptions},
    ConnectOptions,
};
use std::{net::SocketAddr, path::PathBuf, str::FromStr, sync::Arc, time::Duration};
use tracing::log::LevelFilter;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let config = settings::Settings::new().expect("failed to create config");

    let subscriber = telemetry::get_subscriber(&config.log);
    telemetry::init_subscriber(subscriber);

    match settings::test_source_config(&config.sources) {
        Ok(_) => tracing::info!("source config valid:\n{:#?}", &config.sources),
        Err(_) => {
            tracing::error!("source config invalid:\n{:#?}", &config.sources);
            panic!();
        }
    }

    // use sysinfo::{NetworkExt, NetworksExt, ProcessExt, System, SystemExt};
    // let mut sys = sysinfo::System::new_all();
    // loop {
    //     sys.refresh_all();
    //     let sum = sys
    //         .networks()
    //         .iter()
    //         .map(|(interface, data)| {
    //             dbg!(interface);
    //             dbg!(data.received());
    //             data.received()
    //         })
    //         .sum::<u64>();
    //     dbg!(sum);
    //     tokio::time::sleep(Duration::from_secs(1)).await;
    // }
    let db_connect_options = PgConnectOptions::new()
        .host(&config.database.host)
        .port(config.database.port)
        .username(&config.database.user)
        .password(config.database.password.expose_secret())
        .database(&config.database.name)
        .log_statements(LevelFilter::from_str(&config.database.log_level)?)
        .log_slow_statements(LevelFilter::Warn, Duration::from_secs(1))
        .to_owned();

    let pool = PgPoolOptions::new()
        .connect_with(db_connect_options)
        .await?;

    // let pool = PgPool::connect(&config.database.url).await?;
    let db: DynDatabase = Arc::new(pool) as DynDatabase;
    let (sender, receiver) = tokio::sync::watch::channel(PathBuf::default()); // use default event
    if config.sources.from_log {
        let log_dir = config.sources.log_dir.clone();
        let mut receiver_clone = receiver.clone();
        tokio::spawn(async move {
            if let Err(e) = log::init_watcher(&log_dir, sender).await {
                tracing::error!("Directory watcher failed: {:?}", e);
            }
        });
        let db_clone = db.clone();

        tokio::spawn(async move {
            log::persist_logs(&mut receiver_clone, db_clone).await;
        });
    }

    // TODO: spawn task to persist host metrics

    // TODO: spawn task to fetch logs from db with bookmark and send push notification
    // to client in case of warnings/errors

    let app = startup::create_app(config.clone(), db);
    let addr = SocketAddr::from_str(&config.app.address())
        .unwrap_or_else(|_| panic!("failed to parse app address: {}", &config.app.address()));

    tracing::info!("listening on {}", addr);
    if axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .with_graceful_shutdown(shutdown::handle_shutdown())
        .await
        .is_err()
    {
        tracing::error!("failed to bind server address");
        panic!()
    }
    Ok(())
}
