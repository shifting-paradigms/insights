//! Database module
//!
//! This module contains the database type and its implementation.
//!

use crate::metrics::log::Log;
use anyhow::Result;
use sqlx::{postgres::PgPool, types::Json, Acquire};
use time::OffsetDateTime;

/// The injectable database type.
///
/// This type is used across the application.
pub type DynDatabase = std::sync::Arc<dyn Database + Send + Sync>;
/// The database type.
///
/// This type is used to abstract the database implementation.
pub type DatabasePool = PgPool;

/// The database trait.
///
/// This trait is used to abstract the database implementation.
#[async_trait::async_trait]
pub trait Database {
    /// Inserts the given logs into the database.
    async fn insert_logs(&self, logs: Vec<Log>) -> anyhow::Result<()>;
    /// Returns all logs since the given timestamp.
    async fn get_logs_since(&self, timestamp: OffsetDateTime) -> anyhow::Result<Vec<Log>>;
}

#[async_trait::async_trait]
impl Database for DatabasePool {
    async fn insert_logs(&self, logs: Vec<Log>) -> Result<()> {
        fn order_by_timestamp(logs: &mut [Log]) {
            logs.sort_by(|a, b| a.timestamp.cmp(&b.timestamp));
        }
        let mut logs = logs;
        order_by_timestamp(&mut logs);
        let mut conn = self.acquire().await?;
        let mut tx = conn.begin().await?;
        for log in logs {
            let json = Json(log.span);
            sqlx::query(
                r#"INSERT INTO logs (timestamp, level, message, target, span, created_at)
            VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT DO NOTHING"#,
            )
            .bind(log.timestamp)
            .bind(log.level)
            .bind(log.message)
            .bind(log.target)
            .bind(json)
            .bind(log.created_at)
            .execute(&mut tx)
            .await?;
        }
        tx.commit().await?;
        Ok(())
    }

    async fn get_logs_since(&self, timestamp: OffsetDateTime) -> Result<Vec<Log>> {
        let mut conn = self.acquire().await?;
        let logs = sqlx::query_as!(
            Log,
            r#"
                SELECT timestamp, level, message, target, span, created_at
                FROM logs
                WHERE timestamp > $1
            "#,
            timestamp
        )
        .fetch_all(&mut conn)
        .await?;

        Ok(logs)
    }
}
