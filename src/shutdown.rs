//! Graceful shutdown support.
//!
//! This module provides a `handle_shutdown` function that returns a future that
//! resolves when the process should begin shutting down. This future resolves
//! when either a SIGINT or SIGTERM signal is received.

/// Gracefully shutdown the server when a termination signal is received.
#[tracing::instrument]
pub async fn handle_shutdown() {
    let ctrl_c = async {
        tokio::signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }

    tracing::info!("termination signal received, starting graceful shutdown");
}
