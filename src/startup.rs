//! Application startup logic
//!
//! This module contains the logic for starting the application.

use crate::{database::DynDatabase, errors, settings::Settings, websocket};
use axum::{error_handling::HandleErrorLayer, routing::get, Extension, Router};
use secrecy::ExposeSecret;
use tower_http::{auth::RequireAuthorizationLayer, trace::TraceLayer};

/// Create the application instance.
///
/// This is used to create the application instance.
#[tracing::instrument(skip_all)]
pub fn create_app(config: Settings, db: DynDatabase) -> Router {
    tracing::debug!("creating app instance");
    Router::new()
        .route("/insights", get(websocket::handlers::ws_handler))
        .layer(RequireAuthorizationLayer::bearer(
            config.auth.token.expose_secret(),
        ))
        .route("/heartbeat", get(|| async {}))
        .layer(
            tower::ServiceBuilder::new()
                .layer(HandleErrorLayer::new(errors::handle_timeout_error))
                .timeout(std::time::Duration::from_secs(30))
                .layer(Extension(config))
                .layer(Extension(db))
                .layer(TraceLayer::new_for_http()),
        )
        .fallback(errors::handle_404)
}
