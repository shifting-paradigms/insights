//! Websocket handlers
//!
//! This module contains the websocket handlers for the server.

use crate::{
    database::DynDatabase,
    metrics::{
        api::{ApiMetricsHandler, PrometheusApiConfig},
        host::HostMetricsHandler,
        MetricsPackage, MetricsType,
    },
    settings::Settings,
    shutdown,
    websocket::configuration::StreamConfig,
};
use axum::{
    extract::ws::{Message, WebSocket, WebSocketUpgrade},
    response::IntoResponse,
    Extension,
};
use futures::{sink::SinkExt, stream::StreamExt};
use tokio::time::Duration;

/// The websocket handler.
///
/// This is used to handle the websocket upgrade request.
#[tracing::instrument(skip_all)]
pub async fn ws_handler(
    ws: WebSocketUpgrade,
    Extension(config): Extension<Settings>,
    Extension(db): Extension<DynDatabase>,
) -> impl IntoResponse {
    tracing::info!("connecting client");
    ws.on_upgrade(|socket| handle_socket(socket, config, db))
}

/// Handle the websocket.
///
/// This is used to handle the websocket connection.
#[tracing::instrument(skip_all)]
async fn handle_socket(socket: WebSocket, config: Settings, db: DynDatabase) {
    let (mut ws_sink, mut ws_stream) = socket.split();

    let stream_config = StreamConfig::default();
    let mut host_interval = stream_config.host_interval();
    let mut api_interval = stream_config.api_interval();
    let mut log_interval = stream_config.log_interval();
    let mut log_start_timestamp = stream_config.log.start_timestamp;

    let mut host_metrics_handler = HostMetricsHandler::init();
    let api_metrics_handler_config = PrometheusApiConfig {
        uri: config.sources.api_prometheus_exporter_uri.clone(),
        bearer_token: config.sources.api_prometheus_auth_token.clone(),
    };
    let mut api_metrics_handler = match ApiMetricsHandler::new(api_metrics_handler_config) {
        Ok(handler) => handler,
        Err(err) => {
            tracing::error!("failed to create api metrics handler: {}", err);
            return;
        }
    };
    loop {
        tokio::select! {
            _ = host_interval.tick() => {
                // TODO: skip if no host metrics are enabled
                host_metrics_handler.refresh();
                let package = host_metrics_handler.get_package();
                let package_str = serde_json::to_string(&package).unwrap_or_else(|_| {
                    panic!(
                        "failed to parse host metrics: {:?}",
                        &host_metrics_handler.get_package()
                    )
                });

                tracing::debug!("send host metrics package: {:#?}", &package_str);
                if ws_sink.send(Message::Text(package_str)).await.is_err() {
                    tracing::info!("client disconnected");
                    return;
                };
                // sleep to avoid flooding the client
                tokio::time::sleep(Duration::from_millis(50)).await;
            },
            _ = api_interval.tick() => {
                // TODO: skip if no api metrics are enabled
                if api_metrics_handler.fetch().await.is_err() {
                    tracing::error!("failed to fetch api metrics");
                }
                let package = api_metrics_handler.get_package();
                let package_str = serde_json::to_string(&package).unwrap_or_else(|_| {
                    panic!(
                        "failed to parse api metrics: {:?}",
                        &api_metrics_handler.get_package()
                    )
                });
                tracing::debug!("send api metrics package: {:#?}", &package_str);
                if ws_sink.send(Message::Text(package_str)).await.is_err() {
                    tracing::info!("client disconnected");
                    return;
                }
                // sleep to avoid flooding the client
                tokio::time::sleep(Duration::from_millis(50)).await;
            },
            _ = log_interval.tick() => {
                // TODO: skip if no logs are enabled
                let mut logs = match db.get_logs_since(log_start_timestamp).await {
                    Ok(logs) => logs,
                    Err(_) => {
                        tracing::error!("failed to fetch logs");
                        vec![]
                    }
                };
                logs.sort_by(|a, b| a.timestamp.cmp(&b.timestamp));

                let last_timestamp = logs.iter().max_by_key(|log| log.timestamp).map(|log| log.timestamp);
                let mut packages = Vec::with_capacity(logs.len());
                for log in logs.iter() {
                    let log_package = MetricsPackage {
                        id: uuid::Uuid::new_v4().to_string(),
                        kind: MetricsType::LogEntry,
                        data: log
                    };
                    packages.push(log_package);
                }

                let mut packages_str = Vec::with_capacity(logs.len());
                for package in packages.iter() {
                    if let Ok(package_str) = serde_json::to_string(&package) {
                        packages_str.push(package_str);
                    } else {
                        tracing::error!("failed to parse package: {:?}", package);
                    }
                }

                if !packages_str.is_empty() {
                    tracing::debug!("Sending {} log packages", packages_str.len());
                }

                for package in packages_str.into_iter() {
                    tracing::trace!("send log package: {:#?}", &package);
                    if ws_sink.send(Message::Text(package)).await.is_err() {
                        tracing::info!("client disconnected");
                        return;
                    }
                    // sleep to avoid flooding the client
                    tokio::time::sleep(Duration::from_millis(50)).await;
                }

                if let Some(timestamp) = last_timestamp {
                    log_start_timestamp = timestamp;
                }
            },
            v = ws_stream.next() => {
                if let Some(Ok(Message::Text(msg))) = v {
                    tracing::info!("received message: {:?}", msg);
                    if let Ok(config) = serde_json::from_str::<StreamConfig>(&msg) {
                            tracing::debug!("received stream config: {:#?}", config);
                            host_interval = config.host_interval();
                            api_interval = config.api_interval();
                            log_interval = config.log_interval();
                            log_start_timestamp = config.log.start_timestamp;
                        } else {
                            tracing::warn!("Received invalid stream config. Ignoring.");
                        }

                } else {
                    tracing::info!("client disconnected");
                    return;
                }
            },
            _ = shutdown::handle_shutdown() => {
                return;
            }
        }
    }
}

// ------------------------------------------------------------------------------------------------
// Tests
// ------------------------------------------------------------------------------------------------

mod tests {

    #[test]
    fn test_deserialize_stream_config() {
        let json = r#"{
            "host": {
                "interval": 1
            },
            "api": {
                "interval": 2
            },
            "log": {
                "interval": 3,
                "start_timestamp": "1985-04-12T23:20:50.52Z"
            }
        }"#;

        let config: super::StreamConfig =
            serde_json::from_str(json).expect("failed to parse config");

        assert_eq!(config.host.interval, 1);
        assert_eq!(config.api.interval, 2);
        assert_eq!(config.log.interval, 3);
        assert_eq!(
            config.log.start_timestamp,
            time::macros::datetime!(1985-04-12 23:20:50.52 +00:00)
        );
    }

    #[test]
    fn test_deserialize_stream_config_with_defaults() {
        let timestamp =
            time::OffsetDateTime::from_unix_timestamp(10_000_000_000).expect("invalid timestamp");
        let json = r#"{
            "host": {
                "interval": 1
            },
            "api": {
                "interval": 2
            },
            "log": {
                "interval": 3
            }
        }"#;

        let config: super::StreamConfig =
            serde_json::from_str(json).expect("failed to parse config");

        assert_eq!(config.host.interval, 1);
        assert_eq!(config.api.interval, 2);
        assert_eq!(config.log.interval, 3);
        assert_eq!(config.log.start_timestamp, timestamp);
    }

    #[test]
    fn test_deserialize_stream_config_with_defaults_2() {
        let timestamp =
            time::OffsetDateTime::from_unix_timestamp(10_000_000_000).expect("invalid timestamp");
        let json = r#"{
            "host": {
                "interval": 1
            },
            "api": {
                "interval": 2
            }
        }"#;

        let config: super::StreamConfig =
            serde_json::from_str(json).expect("failed to parse config");

        assert_eq!(config.host.interval, 1);
        assert_eq!(config.api.interval, 2);
        assert_eq!(config.log.interval, 1_000_000);
        assert_eq!(config.log.start_timestamp, timestamp);
    }

    #[test]
    fn test_deserialize_stream_config_with_defaults_3() {
        let timestamp = time::macros::datetime!(1985-04-12 23:20:50.52 +00:00);
        let json = r#"{
            "host": {
                "interval": 1
            },
            "api": {
                "interval": 2
            },
            "log": {
                "start_timestamp": "1985-04-12T23:20:50.52Z"
            }
        }"#;

        let config: super::StreamConfig =
            serde_json::from_str(json).expect("failed to parse config");

        assert_eq!(config.host.interval, 1);
        assert_eq!(config.api.interval, 2);
        assert_eq!(config.log.interval, 1_000_000);
        assert_eq!(config.log.start_timestamp, timestamp);
    }

    #[test]
    fn test_deserialize_malformed_json_stream_config_fails() {
        let json = r#"{
            "host": {
                "interval": 1a
            },
            "api": {
                "interval": 2
            },
            "log": {
                "interval": 3,
                "start_timestamp": 4b
            }
        }"#;

        let config: Result<super::StreamConfig, _> = serde_json::from_str(json);

        assert!(config.is_err());
    }

    #[test]
    fn serde_serializes_default_config_correctly() {
        let config = super::StreamConfig::default();
        let expected = r#"{"host":{"interval":1000000},"api":{"interval":1000000},"log":{"interval":1000000,"start_timestamp":"2286-11-20T17:46:40Z"}}"#;

        let json = serde_json::to_string(&config).expect("failed to serialize config");

        assert_eq!(json, expected);
    }
}
