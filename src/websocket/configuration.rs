//! Configuration for the websocket.
//!
//! This module contains the configuration for the websocket stream.
//!

use serde::{Deserialize, Serialize};
use time::OffsetDateTime;
use tokio::time::{Duration, Interval, MissedTickBehavior};

/// Configuration for the websocket stream.
#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct StreamConfig {
    #[serde(default)]
    pub host: HostConfig,
    #[serde(default)]
    pub api: ApiConfig,
    #[serde(default)]
    pub log: LogConfig,
}

/// Configuration for sending [host metrics](crate::metrics::host) into the stream.
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct HostConfig {
    /// The interval in seconds to send host metrics into the stream.
    #[serde(default)]
    pub interval: u64,
}

/// Configuration for sending [API metrics](crate::metrics::api) into the stream.
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ApiConfig {
    /// The interval in seconds to send API metrics into the stream.
    #[serde(default)]
    pub interval: u64,
}

/// Configuration for sending [log metrics](crate::metrics::log) into the stream.
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct LogConfig {
    /// The interval in seconds to send log metrics into the stream.
    #[serde(default = "interval_default")]
    pub interval: u64,
    /// The timestamp to start sending log metrics from.
    ///
    /// This is used to send log metrics from a specific point in time.
    #[serde(default = "start_timestamp_default", with = "time::serde::rfc3339")]
    pub start_timestamp: OffsetDateTime,
}
/// Default value for the interval.
///
/// This is used to set the default value for the interval when using serde.
fn interval_default() -> u64 {
    1_000_000
}
/// Default value for the start timestamp.
///
/// This is used to set the default value for the start timestamp when using serde.
#[tracing::instrument()]
fn start_timestamp_default() -> OffsetDateTime {
    match OffsetDateTime::from_unix_timestamp(10_000_000_000) {
        Ok(timestamp) => timestamp,
        Err(_) => {
            tracing::warn!("Failed to create default start timestamp");
            OffsetDateTime::now_utc()
        }
    }
}

impl Default for HostConfig {
    fn default() -> Self {
        Self {
            interval: 1_000_000,
        }
    }
}

impl Default for ApiConfig {
    fn default() -> Self {
        Self {
            interval: 1_000_000,
        }
    }
}

impl Default for LogConfig {
    fn default() -> Self {
        Self {
            interval: 1_000_000,
            start_timestamp: start_timestamp_default(),
        }
    }
}

impl StreamConfig {
    /// Create an [interval](tokio::time::Interval) for sending host metrics.
    pub fn host_interval(&self) -> Interval {
        let duration = Duration::from_secs(self.host.interval);
        let mut interval = tokio::time::interval(duration);
        interval.set_missed_tick_behavior(MissedTickBehavior::Burst);
        interval.reset();
        interval
    }

    /// Create an [interval](tokio::time::Interval) for sending API metrics.
    pub fn api_interval(&self) -> Interval {
        let duration = Duration::from_secs(self.api.interval);
        let mut interval = tokio::time::interval(duration);
        interval.set_missed_tick_behavior(MissedTickBehavior::Burst);
        interval.reset();
        interval
    }

    /// Create an [interval](tokio::time::Interval) for sending log metrics.
    pub fn log_interval(&self) -> Interval {
        let duration = Duration::from_secs(self.log.interval);
        let mut interval = tokio::time::interval(duration);
        interval.set_missed_tick_behavior(MissedTickBehavior::Burst);
        interval.reset();
        interval
    }
}
