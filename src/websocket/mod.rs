//! Module for the websocket.
//!
//! This module contains the websocket handlers and the configuration
//! for the websocket stream.
//!

pub mod configuration;
pub mod handlers;
