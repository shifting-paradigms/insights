//! Telemetry utilities
//!
//! This module contains utilities for setting up telemetry, including
//! logging and tracing.
//!

use crate::settings::Log;

use tracing::{subscriber::set_global_default, Subscriber};
use tracing_log::LogTracer;
use tracing_subscriber::{fmt, layer::SubscriberExt, EnvFilter, Registry};

/// Get the [tracing filter](tracing_subscriber::filter::EnvFilter).
pub fn get_tracing_filter(level: &str) -> EnvFilter {
    EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new(level))
}

/// Get the [tracing subscriber](tracing::Subscriber).
pub fn get_subscriber(log_config: &Log) -> impl Subscriber + Send + Sync {
    Registry::default()
        .with(get_tracing_filter(&log_config.level))
        .with(fmt::layer().pretty().with_writer(std::io::stdout))
}

/// Initialize the [tracing subscriber](tracing::Subscriber).
///
/// This function initializes the [tracing subscriber](tracing::Subscriber)
/// and the [tracing log](tracing_log::LogTracer) by setting the
/// [global default subscriber](tracing::subscriber::set_global_default).
pub fn init_subscriber(subscriber: impl Subscriber + Send + Sync) {
    LogTracer::init().expect("Failed to set logger");
    set_global_default(subscriber).expect("Failed to set subscriber");
}
