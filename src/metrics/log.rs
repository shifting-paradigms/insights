//! Log metrics
//!
//! This module contains the log metrics type and its implementation.

use crate::database::DynDatabase;
use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    fs::{self, File},
    io::{BufRead, BufReader},
    path::{Path, PathBuf},
    time::{Duration, SystemTime},
};
use time::OffsetDateTime;
use tokio::sync::watch::{Receiver, Sender};

/// The source metrics type.
///
/// This type is used to collect metrics from the source.
#[derive(Deserialize, sqlx::FromRow)]
struct SourceLog {
    /// The timestamp of the log entry.
    timestamp: String,
    /// The level of the log entry.
    level: String,
    /// The fields of the log entry.
    fields: Fields,
    /// The target of the log entry.
    target: Option<String>,
    /// The span of the log entry.
    span: Option<serde_json::Value>,
}

/// The fields of the source metrics type.
#[derive(Deserialize, sqlx::FromRow)]
struct Fields {
    /// The message field of the log entry.
    message: String,
}

/// The log metrics type.
///
/// This type is used to store the log entries in the database
/// and to send them into the stream.
#[derive(Deserialize, sqlx::FromRow, Debug, Clone, Serialize)]
pub struct Log {
    /// The timestamp of the log entry.
    #[serde(with = "time::serde::rfc3339")]
    pub timestamp: OffsetDateTime,
    /// The level of the log entry.
    pub level: String,
    /// The message of the log entry.
    pub message: String,
    /// The target of the log entry.
    pub target: Option<String>,
    /// The span of the log entry.
    pub span: Option<serde_json::Value>,
    /// The time when the log entry was stored in the database.
    ///
    /// This field is used to sort the log entries.
    /// It is also used to determine if the log entry is new
    /// or if it was already sent to the stream.
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: OffsetDateTime,
}

impl From<SourceLog> for Log {
    /// Convert a source log entry into a log entry.
    fn from(source: SourceLog) -> Self {
        let now = OffsetDateTime::now_utc();
        let format = time::format_description::well_known::Rfc3339;
        let timestamp: OffsetDateTime =
            OffsetDateTime::parse(&source.timestamp, &format).expect("failed to parse timestamp");
        Log {
            timestamp,
            level: source.level,
            message: source.fields.message,
            target: source.target,
            span: source.span,
            created_at: now,
        }
    }
}

/// Returns all file paths in a directory.
#[tracing::instrument]
pub fn get_all_file_paths(dir: &str) -> anyhow::Result<Vec<PathBuf>> {
    let mut paths = fs::read_dir(dir)?
        .filter_map(|entry| entry.ok())
        .collect::<Vec<_>>();
    paths.sort_by_key(|p| match p.metadata() {
        Ok(m) => match m.modified() {
            Ok(t) => t,
            Err(e) => {
                tracing::error!("failed to get modified time: {}", e);
                SystemTime::UNIX_EPOCH
            }
        },
        Err(e) => {
            tracing::error!("failed to get metadata: {}", e);
            SystemTime::UNIX_EPOCH
        }
    });
    Ok(paths.iter().map(|p| p.path()).collect())
}

/// Returns the file path in a directory, that has been modified last.
#[tracing::instrument]
pub fn get_latest_file_path(dir: &str) -> anyhow::Result<PathBuf> {
    let mut paths = fs::read_dir(dir)?
        .filter_map(|entry| entry.ok())
        .collect::<Vec<_>>();
    paths.sort_by_key(|p| match p.metadata() {
        Ok(m) => match m.modified() {
            Ok(t) => t,
            Err(e) => {
                tracing::error!("failed to get modified time: {}", e);
                SystemTime::UNIX_EPOCH
            }
        },
        Err(e) => {
            tracing::error!("failed to get metadata: {}", e);
            SystemTime::UNIX_EPOCH
        }
    });
    match paths.pop() {
        Some(path) => Ok(path.path()),
        None => {
            tracing::error!("no file found");
            Err(anyhow::anyhow!("no file found"))
        }
    }
}

/// Create a [watcher](tokio::sync::watch) that watches a directory for new files.
#[tracing::instrument(skip(sender))]
pub async fn init_watcher(dir: &str, sender: Sender<PathBuf>) -> anyhow::Result<(), anyhow::Error> {
    let mut modification_times: HashMap<PathBuf, SystemTime> = HashMap::new();
    while !sender.is_closed() {
        let paths = match get_all_file_paths(dir) {
            Ok(paths) => paths,
            Err(e) => {
                tracing::error!("failed to get file paths: {}", e);
                tokio::time::sleep(Duration::from_secs(1)).await;
                continue;
            }
        };
        for path in paths {
            if let Ok(modified_time) = path.metadata()?.modified() {
                if let Some(v) = modification_times.get(&path) {
                    if v != &modified_time {
                        tracing::debug!("sending file change notification");
                        if sender.send(path.clone()).is_err() {
                            tracing::info!("failed to send path");
                            continue;
                        }
                        modification_times.insert(path, modified_time);
                    }
                } else {
                    tracing::debug!("sending new file creation notification");
                    if sender.send(path.clone()).is_err() {
                        tracing::info!("failed to send path");
                        continue;
                    }
                    modification_times.insert(path, modified_time);
                }
                // this sleep avoids message loss due to channel capacity of 1
                tokio::time::sleep(Duration::from_secs(1)).await;
            }
        }
        // this sleep reduces resource usage
        tokio::time::sleep(Duration::from_secs(1)).await;
    }
    tracing::info!("sender closed");
    Ok(())
}

/// Read all log entries from a file, starting from a given offset.
#[tracing::instrument]
pub fn read_with_offset(log_path: &Path, offset: usize) -> Result<Vec<Log>> {
    let file = File::open(log_path)?;
    let reader = BufReader::with_capacity(1024 * 16, file);
    let mut logs = Vec::new();
    for line in reader.lines().skip(offset) {
        let source_log: SourceLog = serde_json::from_str(&line?)?;
        let log: Log = source_log.into();
        logs.push(log);
    }
    Ok(logs)
}

/// Read all log entries from set of files.
#[tracing::instrument]
pub fn read_all_logs(
    log_file_paths: &Vec<PathBuf>,
    bookmark: &mut HashMap<PathBuf, usize>,
) -> anyhow::Result<Vec<Log>> {
    let mut all_logs: Vec<Log> = Vec::with_capacity(10_000_000);
    for path in log_file_paths.iter() {
        let file = File::open(path)?;
        let reader = BufReader::new(file);
        for line in reader.lines() {
            let source_log: SourceLog = serde_json::from_str(&line?)?;
            let log: Log = source_log.into();
            all_logs.push(log);
            if let Some(v) = bookmark.get_mut(path) {
                *v += 1;
            };
        }
    }
    Ok(all_logs)
}

/// Stores new log entries in the database.
///
/// This function waits for notifications from a [watcher](tokio::sync::watch).
/// The corresponding watcher is created by [init_watcher](init_watcher).
#[tracing::instrument(skip(db, receiver))]
pub async fn persist_logs(receiver: &mut Receiver<PathBuf>, db: DynDatabase) {
    let mut bookmark: HashMap<PathBuf, usize> = HashMap::new();
    while receiver.changed().await.is_ok() {
        tracing::debug!("received file change notification");
        let pathbuf = receiver.borrow().to_path_buf();
        let offset = *bookmark.get(&pathbuf).unwrap_or(&0);
        let logs = match read_with_offset(&pathbuf, offset) {
            Ok(logs) => logs,
            Err(e) => {
                tracing::error!("Failed to read with offset: {:?}", e);
                return;
            }
        };
        bookmark.insert(pathbuf, offset + logs.len());
        match db.insert_logs(logs).await {
            Ok(_) => {}
            Err(e) => {
                tracing::error!("Failed to insert logs: {:?}", e);
                return;
            }
        }
    }
}
