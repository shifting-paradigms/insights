//! Metrics module
//!
//! This module contains the metrics that are sent into the websocket stream.
//!

use serde::{Deserialize, Serialize};

pub mod api;
pub mod host;
pub mod log;

/// A package of metrics.
///
/// This is used to send metrics into the websocket stream.
#[derive(Debug, Deserialize, Serialize)]
pub struct MetricsPackage<T> {
    pub id: String,
    pub kind: MetricsType,
    pub data: T,
}

/// The type of metrics.
///
/// This is used to determine what type of metrics are being sent into the websocket stream.
#[derive(Debug, Deserialize, Serialize)]
pub enum MetricsType {
    ApiMetrics,
    HostMetrics,
    LogEntry,
}
