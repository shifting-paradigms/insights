//! This module contains the implementation of the api metrics handler.

use hyper::{client::connect::HttpConnector, Body, Client, Request};
use secrecy::{ExposeSecret, SecretString};
use serde::Serialize;

/// The api metrics handler.
///
/// This is used to fetch the api metrics from the prometheus endpoint.
pub struct ApiMetricsHandler {
    /// The http client.
    client: Client<HttpConnector>,
    /// The configuration of the prometheus endpoint.
    config: PrometheusApiConfig,
    /// The response of the prometheus endpoint.
    ///
    /// This is used to store the response of the prometheus endpoint.
    response: Option<String>,
    /// The metrics.
    ///
    /// This is used to store the parsed response of the prometheus endpoint.
    metrics: ApiMetrics,
}

/// Configuration for the api metrics handler.
pub struct PrometheusApiConfig {
    /// The uri of the prometheus endpoint.
    pub uri: String,
    /// The bearer token for the authentication.
    pub bearer_token: SecretString,
}

impl ApiMetricsHandler {
    /// Create a new api metrics handler.
    pub fn new(config: PrometheusApiConfig) -> anyhow::Result<Self> {
        let handler = ApiMetricsHandler {
            client: Client::new(),
            config,
            response: None,
            metrics: ApiMetrics {
                http_requests_total: Vec::new(),
                http_requests_duration_seconds: Vec::new(),
            },
        };
        Ok(handler)
    }

    /// Fetch the metrics.
    #[tracing::instrument(skip_all)]
    pub async fn fetch(&mut self) -> anyhow::Result<()> {
        let header_value = format!("Bearer {}", &self.config.bearer_token.expose_secret());
        let req = Request::builder()
            .method("GET")
            .uri(&self.config.uri)
            .header("Authorization", &header_value)
            .body(Body::empty())
            .unwrap();
        let res = self.client.request(req).await;
        let res = match res {
            Ok(res) => res,
            Err(err) => {
                tracing::error!("failed to fetch prometheus endpoint: {}", err);
                return Err(anyhow::anyhow!("{}", err));
            }
        };
        if !res.status().is_success() {
            tracing::error!("failed to fetch prometheus endpoint: {}", res.status());
            return Err(anyhow::anyhow!("{}", res.status()));
        }
        let buf = hyper::body::to_bytes(res).await;
        let buf = match buf {
            Ok(buf) => buf,
            Err(err) => {
                tracing::error!("failed to parse body: {}", err);
                return Err(anyhow::anyhow!("{}", err));
            }
        };
        let response = String::from_utf8(buf.to_vec());
        self.response = match response {
            Ok(response) => Some(response),
            Err(err) => {
                tracing::error!("failed to read body into string: {}", err);
                return Err(anyhow::anyhow!("{}", err));
            }
        };
        self.parse();
        Ok(())
    }

    /// Create a metrics package from the stored metrics.
    pub fn get_package(&self) -> super::MetricsPackage<&ApiMetrics> {
        tracing::debug!("create host metrics package");
        super::MetricsPackage {
            id: uuid::Uuid::new_v4().to_string(),
            kind: super::MetricsType::ApiMetrics,
            data: &self.metrics,
        }
    }

    /// Parse the stored response.
    #[tracing::instrument(skip_all)]
    fn parse<'a>(&'a mut self) {
        if let Some(res) = &self.response {
            let data = extract_data_points(res);

            self.metrics = ApiMetrics {
                http_requests_total: Vec::with_capacity(data.len()),
                http_requests_duration_seconds: Vec::with_capacity(data.len()),
            };

            for line in data {
                let metric =
                    PrometheusMetric::try_from_exposition_format(line).unwrap_or_else(|err| {
                        tracing::error!("failed to parse prometheus metric: {}", err);
                        panic!("{}", err);
                    });

                if metric.name.starts_with("http_requests_total") {
                    self.metrics.http_requests_total.push(metric);
                } else if metric.name.starts_with("http_requests_duration_seconds") {
                    self.metrics.http_requests_duration_seconds.push(metric);
                } else {
                    tracing::error!("unknown metric name: {}", metric.name);
                }
            }
        }
    }
}

/// Contains different types on prometheus metrics.
#[derive(Debug, Serialize)]
pub struct ApiMetrics {
    http_requests_total: Vec<PrometheusMetric>,
    http_requests_duration_seconds: Vec<PrometheusMetric>,
}

/// Contains the data points from the prometheus exposition format.
#[derive(Debug, Serialize)]
struct PrometheusMetric {
    name: String,
    method: String,
    path: String,
    status: u16,
    le: Option<f64>,
    count: f64,
}

impl PrometheusMetric {
    /// Try to create a new prometheus metric from the exposition format.
    fn try_from_exposition_format(line: &str) -> Result<Self, std::fmt::Error> {
        fn try_le(key_pair: &str) -> anyhow::Result<f64> {
            let le = key_pair
                .strip_prefix("le=\"")
                .expect("failed to strip prefix")
                .strip_suffix('"')
                .expect("failed to strip suffix")
                .parse()?;
            Ok(le)
        }
        let (prefix, count) = line
            .trim()
            .split_once("} ")
            .unwrap_or_else(|| panic!("failed to split string: {}", line));
        let count: f64 = count.parse().expect("failed to parse count");
        let (name, key_pairs) = prefix
            .split_once('{')
            .unwrap_or_else(|| panic!("failed to split string: {}", prefix));
        let key_pairs: Vec<&str> = key_pairs.split(',').collect();
        let method: &str = key_pairs[0]
            .strip_prefix("method=\"")
            .expect("failed to strip prefix a")
            .strip_suffix('"')
            .expect("failed to strip suffix a");
        let path: &str = key_pairs[1]
            .strip_prefix("path=\"")
            .expect("failed to strip prefix a")
            .strip_suffix('"')
            .expect("failed to strip suffix a");
        let status: u16 = key_pairs[2]
            .strip_prefix("status=\"")
            .expect("failed to strip prefix a")
            .strip_suffix('"')
            .expect("failed to strip suffix")
            .parse()
            .expect("failed to parse status");
        let le: Option<f64> = if key_pairs.len() == 4 {
            match try_le(key_pairs[3]) {
                Ok(le) => Some(le),
                Err(_) => None,
            }
        } else {
            None
        };
        Ok(PrometheusMetric {
            name: name.to_string(),
            method: method.to_string(),
            path: path.to_string(),
            status,
            le,
            count,
        })
    }
}

/// Extract the data points from the response string.
fn extract_data_points(response: &str) -> Vec<&str> {
    response
        .trim()
        .split('\n')
        .map(|line| line.trim())
        .filter(|line| !line.trim().starts_with('#') && line.len().ne(&0))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn extracts_correct_amount_of_data_points_from_response_string() {
        let string = r#"
            # TYPE http_requests_total counter
            http_requests_total{method="GET",path="/heartbeat",status="200"} 1
            http_requests_total{method="GET",path="/metrics",status="200"} 89
            http_requests_total{method="POST",path="/subscriptions",status="201"} 4

            # TYPE http_requests_duration_seconds histogram
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="0.005"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="0.01"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="0.025"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="0.05"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="0.1"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="0.25"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="0.5"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="1"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="2.5"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="5"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="10"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="20"} 89
            http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="+Inf"} 89
            http_requests_duration_seconds_sum{method="GET",path="/metrics",status="200"} 0.09396177100000004
            http_requests_duration_seconds_count{method="GET",path="/metrics",status="200"} 89
        "#;
        let result = extract_data_points(&string);
        assert_eq!(result.len(), 18);
    }

    #[test]
    fn parses_total_requests() {
        let line = r#"http_requests_total{method="GET",path="/heartbeat",status="200"} 1"#;
        let result = PrometheusMetric::try_from_exposition_format(line)
            .expect("failed to parse prometheus metrics");
        assert_eq!(result.name, "http_requests_total");
        assert_eq!(result.method, "GET");
        assert_eq!(result.path, "/heartbeat");
        assert_eq!(result.status, 200);
        assert_eq!(result.le, None);
        assert_eq!(result.count, 1.0);
    }

    #[test]
    fn parses_duration_bucket() {
        let line = r#"http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="0.005"} 89"#;
        let result = PrometheusMetric::try_from_exposition_format(line)
            .expect("failed to parse prometheus metrics");
        assert_eq!(result.name, "http_requests_duration_seconds_bucket");
        assert_eq!(result.method, "GET");
        assert_eq!(result.path, "/metrics");
        assert_eq!(result.status, 200);
        assert_eq!(result.le, Some(0.005));
        assert_eq!(result.count, 89.0);
    }

    #[test]
    fn parses_duration_bucket_with_inf() {
        let line = r#"http_requests_duration_seconds_bucket{method="GET",path="/metrics",status="200",le="+Inf"} 89"#;
        let result = PrometheusMetric::try_from_exposition_format(line)
            .expect("failed to parse prometheus metrics");
        assert_eq!(result.name, "http_requests_duration_seconds_bucket");
        assert_eq!(result.method, "GET");
        assert_eq!(result.path, "/metrics");
        assert_eq!(result.status, 200);
        assert_eq!(result.le, Some(f64::INFINITY));
        assert_eq!(result.count, 89.0);
    }

    #[test]
    fn parses_duration_sum() {
        let line = r#"http_requests_duration_seconds_sum{method="GET",path="/metrics",status="200"} 0.09396177100000004"#;
        let result = PrometheusMetric::try_from_exposition_format(line)
            .expect("failed to parse prometheus metrics");
        assert_eq!(result.name, "http_requests_duration_seconds_sum");
        assert_eq!(result.method, "GET");
        assert_eq!(result.path, "/metrics");
        assert_eq!(result.status, 200);
        assert_eq!(result.le, None);
        assert_eq!(result.count, 0.09396177100000004);
    }

    #[test]
    fn parses_duration_count() {
        let line =
            r#"http_requests_duration_seconds_count{method="GET",path="/metrics",status="200"} 89"#;
        let result = PrometheusMetric::try_from_exposition_format(line)
            .expect("failed to parse prometheus metrics");
        assert_eq!(result.name, "http_requests_duration_seconds_count");
        assert_eq!(result.method, "GET");
        assert_eq!(result.path, "/metrics");
        assert_eq!(result.status, 200);
        assert_eq!(result.le, None);
        assert_eq!(result.count, 89.0);
    }
}
