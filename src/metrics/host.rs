//! Host metrics handler
//!
//! This module contains the host metrics handler.
//!
//! The host metrics handler is used to collect host metrics and send them into the websocket stream.

use serde::{Deserialize, Serialize};
use std::time::SystemTime;
use sysinfo::{DiskExt, NetworkExt, NetworksExt, ProcessorExt, RefreshKind, System, SystemExt};

/// Host metrics handler
pub struct HostMetricsHandler {
    /// The system information
    pub system: sysinfo::System,
    /// The specifics to refresh
    specifics: sysinfo::RefreshKind,
    /// The host metrics
    pub metrics: HostMetrics,
}

impl HostMetricsHandler {
    /// Create a new host metrics handler.
    pub fn init() -> Self {
        tracing::debug!("initializing host metrics handler");
        let specifics = RefreshKind::new()
            .with_cpu()
            .with_memory()
            .with_disks_list()
            .with_networks_list();
        let system = System::new_with_specifics(specifics);

        HostMetricsHandler {
            metrics: HostMetrics::new(&system),
            specifics,
            system,
        }
    }

    /// Refresh the host metrics.
    pub fn refresh(&mut self) {
        tracing::debug!("refresh host metrics");
        self.system.refresh_specifics(self.specifics);
        self.metrics.refresh(&self.system);
    }

    /// Create a package containing the host metrics.
    pub fn get_package(&self) -> super::MetricsPackage<&HostMetrics> {
        tracing::debug!("create host metrics package");
        super::MetricsPackage {
            id: uuid::Uuid::new_v4().to_string(),
            kind: super::MetricsType::HostMetrics,
            data: &self.metrics,
        }
    }
}

/// Host metrics
#[derive(Debug, Deserialize, Serialize)]
pub struct HostMetrics {
    timestamp: u64,
    name: String,
    os_version: String,
    kernel: String,
    hostname: String,
    uptime: u64,
    cpu: CpuInfo,
    memory: MemInfo,
    disk: DiskInfo,
    io: IoInfo,
}

/// CPU metrics
#[derive(Debug, Deserialize, Serialize)]
struct CpuInfo {
    brand: String,
    cores: usize,
    usage: f32,
    avg_load_one: f32,
    avg_load_five: f32,
    avg_load_fifteen: f32,
}

/// Memory metrics
#[derive(Debug, Deserialize, Serialize)]
struct MemInfo {
    total: u64,
    used: u64,
    free: u64,
    available: u64,
}

/// Disk metrics
#[derive(Debug, Deserialize, Serialize)]
struct DiskInfo {
    total: u64,
    used: u64,
}

/// IO metrics
#[derive(Debug, Deserialize, Serialize)]
struct IoInfo {
    received: u64,
    transmitted: u64,
}

impl HostMetrics {
    /// Initiallize the host metrics.
    pub fn new(sys: &System) -> Self {
        HostMetrics {
            timestamp: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .expect("failed to calc duration")
                .as_secs(),
            name: sys.name().unwrap_or_default(),
            os_version: sys.long_os_version().unwrap_or_default(),
            kernel: sys.kernel_version().unwrap_or_default(),
            hostname: sys.host_name().unwrap_or_default(),
            uptime: sys.uptime(),
            cpu: CpuInfo {
                brand: sys.global_processor_info().brand().to_string(),
                cores: sys.physical_core_count().unwrap_or(0),
                // % with 2 digits
                usage: (sys.global_processor_info().cpu_usage() * 100.0).round() / 100.0,
                // count with 2 digits
                avg_load_one: ((sys.load_average().one * 100.0).round() / 100.0) as f32,
                // count with 2 digits
                avg_load_five: ((sys.load_average().five * 100.0).round() / 100.0) as f32,
                // count with 2 digits
                avg_load_fifteen: ((sys.load_average().fifteen * 100.0).round() / 100.0) as f32,
            },
            memory: MemInfo {
                // MB
                total: (sys.total_memory()) / 1000,
                used: (sys.used_memory()) / 1000,
                free: sys.free_memory() / 1000,
                available: sys.available_memory() / 1000,
            },
            disk: DiskInfo {
                // MB
                total: (sys
                    .disks()
                    .iter()
                    .map(|disk| disk.total_space())
                    .sum::<u64>()
                    / 1_000_000),
                used: (sys
                    .disks()
                    .iter()
                    .map(|disk| disk.total_space() - disk.available_space())
                    .sum::<u64>()
                    / 1_000_000),
            },
            io: IoInfo {
                // B
                received: sys.networks().iter().map(|(_, data)| data.received()).sum(),
                transmitted: sys
                    .networks()
                    .iter()
                    .map(|(_, data)| data.transmitted())
                    .sum(),
            },
        }
    }

    /// Refresh the host metrics.
    pub fn refresh(&mut self, sys: &System) {
        self.timestamp = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .expect("failed to calc duration")
            .as_secs();
        self.uptime = sys.uptime();
        self.cpu.usage = (sys.global_processor_info().cpu_usage() * 100.0).round() / 100.0;
        self.cpu.avg_load_one = ((sys.load_average().one * 100.0).round() / 100.0) as f32;
        self.cpu.avg_load_five = ((sys.load_average().five * 100.0).round() / 100.0) as f32;
        self.cpu.avg_load_fifteen = ((sys.load_average().fifteen * 100.0).round() / 100.0) as f32;
        self.memory.used = sys.used_memory() / 1000;
        self.memory.free = sys.free_memory() / 1000;
        self.memory.available = sys.available_memory() / 1000;
        self.disk.used = sys
            .disks()
            .iter()
            .map(|disk| disk.total_space() - disk.available_space())
            .sum::<u64>()
            / 1_000_000;
        self.io.received = sys.networks().iter().map(|(_, data)| data.received()).sum();
        self.io.transmitted = sys
            .networks()
            .iter()
            .map(|(_, data)| data.transmitted())
            .sum();
    }
}
